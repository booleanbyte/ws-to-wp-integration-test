package com.booleanbyte.worldpainter.worldsynth.integration;

import net.worldsynth.synth.Synth;

public class InvalidSynthException extends Exception {
	private static final long serialVersionUID = 1L;
	
	private final Synth synth;
	
	public InvalidSynthException(String message, Synth synth) {
		super(message);
		this.synth = synth;
	}
	
	public Synth getSynth() {
		return synth;
	}
}
