package com.booleanbyte.worldpainter.worldsynth.integration;

public class NoValidSynthException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public NoValidSynthException(String message) {
		super(message);
	}
}
