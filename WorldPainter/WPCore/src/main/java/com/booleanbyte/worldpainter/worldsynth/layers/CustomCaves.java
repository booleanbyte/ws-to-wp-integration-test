package com.booleanbyte.worldpainter.worldsynth.layers;

import com.booleanbyte.worldpainter.worldsynth.integration.SynthWrapper;
import org.pepsoft.worldpainter.layers.Layer;

public class CustomCaves extends Layer {
    private static final long serialVersionUID = 1L;

    public static final CustomCaves INSTANCE = new CustomCaves();

    private static SynthWrapper synthWrapper;

    protected CustomCaves() {
        super("com.booleanbyte.customcaves", "Custom caves", "Generate custom underground caves", DataSize.NIBBLE, 101);
    }

    public static void setSynthWrapper(SynthWrapper synthWrapper) {
        CustomCaves.synthWrapper = synthWrapper;
    }

    public static SynthWrapper getSynthWrapper() {
        return CustomCaves.synthWrapper;
    }
}
