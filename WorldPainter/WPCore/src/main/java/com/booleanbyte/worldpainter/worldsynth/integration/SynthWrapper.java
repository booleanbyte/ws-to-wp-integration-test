package com.booleanbyte.worldpainter.worldsynth.integration;

import net.worldsynth.build.Build;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.extent.BuildExtent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.synth.Synth;

import java.util.Hashtable;

public class SynthWrapper {
	
	private final Synth synth;
	
	private final Hashtable<String, AbstractModule> synthParameters = new Hashtable<String, AbstractModule>();
	private final ModuleWrapper wpExit;
	
	public SynthWrapper(Synth synth) throws InvalidSynthException {
		this.synth = synth;
		
		wpExit = getWpExit(synth);
	}
	
	public Synth getSynth() {
		return synth;
	}
	
	public String getName() {
		return synth.getName();
	}
	
	public DatatypeBlockspace getChunkBlockspace(int x, int z, int height) {
		DatatypeBlockspace blockspace = new DatatypeBlockspace(new BuildExtent(x*16, 0, z*16, 16, height, 16), 1.0);
		ModuleOutputRequest request = new ModuleOutputRequest(wpExit.module.getOutput(0), blockspace);
		blockspace = (DatatypeBlockspace) Build.getModuleOutput(wpExit, request, null);
		return blockspace;
	}
	
	private ModuleWrapper getWpExit(Synth synth) throws InvalidSynthException {
		for(ModuleWrapper moduleWrapper: synth.getPatch().getModuleWrapperList()) {
			if(moduleWrapper.getCustomName().equals("wpExit")) {
				return moduleWrapper;
			}
		}
		throw new InvalidSynthException("Invalid synth, missing wpExit", synth);
	}
}
