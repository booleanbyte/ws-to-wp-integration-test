package com.booleanbyte.worldpainter.worldsynth.layers.exporters;

import com.booleanbyte.worldpainter.worldsynth.layers.CustomCaves;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.material.MaterialState;
import org.pepsoft.minecraft.Chunk;
import org.pepsoft.worldpainter.Dimension;
import org.pepsoft.worldpainter.Platform;
import org.pepsoft.worldpainter.Tile;
import org.pepsoft.worldpainter.exporting.AbstractLayerExporter;
import org.pepsoft.worldpainter.exporting.FirstPassLayerExporter;
import org.pepsoft.worldpainter.layers.exporters.ExporterSettings;

import static org.pepsoft.minecraft.Material.AIR;

public class CustomCavesExporter extends AbstractLayerExporter<CustomCaves> implements FirstPassLayerExporter {
    public CustomCavesExporter() {
        super(CustomCaves.INSTANCE, new CavernsSettings());
    }
    
    @Override
    public void render(Dimension dimension, Tile tile, Chunk chunk, Platform platform) {
        final CavernsSettings settings = (CavernsSettings) getSettings();
        final long seed = dimension.getSeed();
        final int xOffset = (chunk.getxPos() & 7) << 4;
        final int zOffset = (chunk.getzPos() & 7) << 4;

        int maxY = 0;
        for (int x = 0; x < 16; x++) {
            for (int z = 0; z < 16; z++) {
                final int localX = xOffset + x, localY = zOffset + z;
                maxY = Math.max(maxY, tile.getIntHeight(localX, localY)+1);
            }
        }

        final DatatypeBlockspace caveBlockspace = CustomCaves.getSynthWrapper().getChunkBlockspace(chunk.getxPos(), chunk.getzPos(), maxY);

        for (int x = 0; x < 16; x++) {
            for (int z = 0; z < 16; z++) {
                final int localX = xOffset + x, localY = zOffset + z;

                if (tile.getBitLayerValue(org.pepsoft.worldpainter.layers.Void.INSTANCE, localX, localY)) {
                    continue;
                }

                final int cavernsValue = tile.getLayerValue(CustomCaves.INSTANCE, localX, localY);
                if (cavernsValue > 0) {
                    final int terrainheight = tile.getIntHeight(localX, localY);
                    for (int y = terrainheight; y >= 0; y--) {
                        if (chunk.getMaterial(x, y, z) == AIR) {
                            continue;
                        }

                        MaterialState<?, ?> ms = caveBlockspace.getLocalMaterial(x, y, z);
                        if (ms.isAir()) {
                            chunk.setMaterial(x, y, z, AIR);
                        }
                    }
                }
            }
        }
    }

    public static class CavernsSettings implements ExporterSettings {
        @Override
        public boolean isApplyEverywhere() {
            return false;
        }

        @Override
        public CustomCaves getLayer() {
            return CustomCaves.INSTANCE;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final CavernsSettings other = (CavernsSettings) obj;
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            return hash;
        }

        @Override
        public CavernsSettings clone() {
            try {
                return (CavernsSettings) super.clone();
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException(e);
            }
        }
        
        private static final long serialVersionUID = 2011060801L;
    }
}