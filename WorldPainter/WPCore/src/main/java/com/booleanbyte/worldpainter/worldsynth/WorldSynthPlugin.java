package com.booleanbyte.worldpainter.worldsynth;

import com.booleanbyte.worldpainter.worldsynth.integration.InvalidSynthException;
import com.booleanbyte.worldpainter.worldsynth.integration.NoValidSynthException;
import com.booleanbyte.worldpainter.worldsynth.integration.SynthWrapper;
import com.booleanbyte.worldpainter.worldsynth.layers.CustomCaves;
import net.worldsynth.WorldSynth;
import net.worldsynth.WorldSynthDirectoryConfig;
import net.worldsynth.addon.IAddon;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.SynthManager;
import org.pepsoft.worldpainter.Configuration;
import org.pepsoft.worldpainter.layers.Layer;
import org.pepsoft.worldpainter.plugins.AbstractPlugin;
import org.pepsoft.worldpainter.plugins.LayerProvider;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WorldSynthPlugin extends AbstractPlugin implements LayerProvider {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(WorldSynthPlugin.class);

    private final File worldSynthDir;
    private final File synthDir;

    public WorldSynthPlugin() {
        super("WorldSynthPlugin", "0.0");

        // Get WorldPainter config directory
        File wpConfigDir = Configuration.getConfigDir();

        // Get the WorldSynth configuration directory
        worldSynthDir = new File(wpConfigDir, "worldsynth");
        if(!worldSynthDir.exists()) {
            worldSynthDir.mkdir();
        }

        // Get the synth directory
        synthDir = new File(worldSynthDir, "synths");
        if(!synthDir.exists()) {
            synthDir.mkdir();
        }

        initWsEngine();

        List<SynthWrapper> synths = getSynths(synthDir);

        try {
            SynthWrapper caveSynth = synths.get(0);
            CustomCaves.setSynthWrapper(caveSynth);
        } catch (IndexOutOfBoundsException e) {
            throw new NoValidSynthException("No valid synth was found for the custom caves layer");
        }
    }

    private void initWsEngine() {
        // Define the WorldSynth directory configuration, this provides information on
        // where too look for WorldSynth resources like material and biome definitions
        // as well as WorldSynth addons.
        // If it is preferred to load materials and biomes from another place from a
        // working repository, change the commenting and define the path to the desired
        // matrials and biomes directories to use.
        File materialsDirectory = null;
//        File materialsDirectory = new File( "C:/Users/.../worldsynth-materials-minecraft/materials");
        File biomesDirectory = null;
//        File biomesDirectory = new File("C:/Users/.../worldsynth-materials-minecraft/biomes");
        WorldSynthDirectoryConfig directoryConfig = new WorldSynthDirectoryConfig(worldSynthDir, null, materialsDirectory, biomesDirectory);

        // Provide any WorldSynth addons that are included as dependencies of the project
        // There are none in this project
        IAddon[] injectedAddons = { };

        // Initialize the WorldSynth engine
        new WorldSynth(directoryConfig, injectedAddons);

        logger.info("Done initializing WorldSynth Engine");
    }

    private static ArrayList<SynthWrapper> getSynths(File synthsDir) {
        ArrayList<SynthWrapper> synths = new ArrayList<SynthWrapper>();

        for(File f: synthsDir.listFiles()) {
            if(!f.getName().endsWith(".wsynth")) {
                continue;
            }

            Synth s = SynthManager.getSynth(f);
            try {
                logger.info("Importing \"" + s.getName() + "\"");
                synths.add(new SynthWrapper(s));
            } catch (InvalidSynthException e) {
                logger.warn("Importing \"" + s.getName() + "\" failed", e);
            }
        }

        return synths;
    }

    @Override
    public List<Layer> getLayers() {
        return Arrays.asList(CustomCaves.INSTANCE);
    }
}
